#!/bin/bash
#sudo su root -
#apt-get update
#snap install docker
#docker pull davblack/mycv:1.0.0
#docker run -dit --name mycv -p 80:80 davblack/mycv:1.0.0
#apt install nginx 
#echo "Welcome from Terraform managed EC2" > /usr/share/nginx/index.html
#sudo systemctl restart nginx
sudo su root -
username=tux
# Create User
useradd -s /bin/bash -c "${username}" -m ${username}
usermod --password passw0rd ${username}
# Set sudo
echo "${username} ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
# Deploy SSH keys
mkdir /home/${username}/.ssh
cat <<EOF | tee -a /home/${username}/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDPpxuFYcPzi2GwysNNeDARjzcpg59mtpUir7cCu2t/IyuqWSfDH3KYUsetaCNLxncsUreov1fBf2O6mHVxtt9NorIqvjjEuSAL4sy/if+c2yS2iFUvJ0cxtsPN3TQGkdJFx6wAko+eCRpTXbrBq0Fq2xwNRTkAhFQGn+e+RwtWM0SfzU4WQappfweyDBSyIosCvHJNisTEQkS02Xekt0PsY4eUI4Q+LK5poARJf9XYDc6oPQSnIBjd9sOLilzSiTQzEODiOKOlnA3WPzdgLk0MNguEWXEeAdldiYVGvJdjt6Xftymw1iOb9bxjNRPcVjAN2Ip67VuZVblRN6Mz6fgUzExvRKcpsW5yOmVx9p1jH0sm4TEANkocPXK3sWMY9LABu3WNY4y1Q7yPcr1nWHDfQy6U60xG0YiKmiY66WdHdKmOOwAy1kWTOy5akQCCN77b6/c7c5vuxjUBUOKN/Juj5EGEeli3TQoRXR/02O6K3Z72VoG0jXiEjT1shEDrij0= david@DESKTOP-E1SPKSJ
EOF
# Set proper permissions
chown -R ${username}:${username} /home/${username}/.ssh
chmod 700 /home/${username}/.ssh
chmod 600 /home/${username}/.ssh/authorized_keys
##

apt install -y nginx

echo "Welcome from Terraform managed EC2" > /var/www/html/index.html
#sudo systemctl restart nginx
